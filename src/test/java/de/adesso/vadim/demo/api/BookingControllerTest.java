package de.adesso.vadim.demo.api;

import de.adesso.vadim.demo.exception.InvalidRangeException;
import de.adesso.vadim.demo.exception.InvalidRoomNumberException;
import de.adesso.vadim.demo.exception.NoRoomAvailableException;
import de.adesso.vadim.demo.model.BookingResult;
import de.adesso.vadim.demo.model.Range;
import de.adesso.vadim.demo.model.RangeAndRoomNumber;
import de.adesso.vadim.demo.model.SimpleNumber;
import de.adesso.vadim.demo.service.ReservationService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.ui.ExtendedModelMap;
import org.springframework.ui.Model;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.mvc.support.RedirectAttributesModelMap;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(MockitoExtension.class)
public class BookingControllerTest {

    @Mock
    ReservationService reservationService;

    @InjectMocks
    BookingController testee;

    @Test
    public void should_test_bookingForm() {
        //GIVEN
        Model model = new ExtendedModelMap();
        List<BookingResult> mockedResults = new ArrayList<>();
        mockedResults.add(new BookingResult(1, 2, true, 12, ""));
        Mockito.when(reservationService.getBookingResults()).thenReturn(mockedResults);

        //WHEN
        String result = testee.bookingForm(model, "bookingExceptionString", "availabilityInfoString",
                "availabilityExceptionString");

        //THEN
        assertThat(result).isEqualTo("booking");
        assertThat(model.asMap().size()).isEqualTo(7);
        assertThat(model.getAttribute("maxrooms")).isNotNull();
        assertThat(model.getAttribute("maxrooms")).isEqualTo(new SimpleNumber());
        assertThat(model.getAttribute("range")).isNotNull();
        assertThat(model.getAttribute("range")).isEqualTo(new Range());
        assertThat(model.getAttribute("availabilitychecker")).isNotNull();
        assertThat(model.getAttribute("availabilitychecker")).isEqualTo(new RangeAndRoomNumber());
        assertThat(model.getAttribute("bookingResults")).isNotNull();
        assertThat(model.getAttribute("bookingResults")).isEqualTo(mockedResults);
        assertThat(model.getAttribute("bookingexception")).isNotNull();
        assertThat(model.getAttribute("bookingexception")).isEqualTo("bookingExceptionString");
        assertThat(model.getAttribute("availabilityinfo")).isNotNull();
        assertThat(model.getAttribute("availabilityinfo")).isEqualTo("availabilityInfoString");
        assertThat(model.getAttribute("availabilityexception")).isNotNull();
        assertThat(model.getAttribute("availabilityexception")).isEqualTo("availabilityExceptionString");
        Mockito.verify(reservationService, Mockito.times(1)).getBookingResults();
    }


    @Test
    public void should_test_getCurrentlyBooked() {
        //GIVEN
        Model model = new ExtendedModelMap();
        boolean[][] bookedDays = new boolean[2][];
        Mockito.when(reservationService.allRoomBookedDays()).thenReturn(bookedDays);

        //WHEN
        String result = testee.getCurrentlyBooked(model);

        //THEN
        assertThat(result).isEqualTo("bookingtable");
        assertThat(model.asMap().size()).isEqualTo(1);
        assertThat(model.getAttribute("allbookeddays")).isNotNull();
        assertThat(model.getAttribute("allbookeddays")).isEqualTo(bookedDays);
        Mockito.verify(reservationService, Mockito.times(1)).allRoomBookedDays();
    }


    @ParameterizedTest
    @ValueSource(ints = {1, 0, 1000, 1001, -1, Integer.MAX_VALUE, Integer.MIN_VALUE})
    public void should_test_setMaxRooms(int numberOfRooms) {
        //GIVEN
        SimpleNumber simpleNumber = new SimpleNumber(numberOfRooms);

        //WHEN
        String result = testee.setMaxRooms(simpleNumber);

        //THEN
        assertThat(result).isEqualTo("redirect:");
        Mockito.verify(reservationService, Mockito.times(1)).setMaximumNumberOfRoomsAndClearBookings(numberOfRooms);
    }


    @Test
    public void should_test_bookingSubmit_correct() throws NoRoomAvailableException, InvalidRangeException {
        //GIVEN
        Range range = new Range(0, 364);
        RedirectAttributes redirectAttrs = new RedirectAttributesModelMap();

        //WHEN
        String result = testee.bookingSubmit(range, redirectAttrs);

        //THEN
        assertThat(result).isEqualTo("redirect:");
        assertThat(redirectAttrs.asMap().size()).isEqualTo(0);
        Mockito.verify(reservationService, Mockito.times(1)).bookARoom(range);
    }

    @Test
    public void should_test_bookingSubmit_exception() throws NoRoomAvailableException, InvalidRangeException {
        //GIVEN
        Range range = new Range(0, 364);
        RedirectAttributes redirectAttrs = new RedirectAttributesModelMap();
        Mockito.doThrow(new InvalidRangeException(new Range(-1, 5))).when(reservationService).bookARoom(Mockito.any());

        //WHEN
        String result = testee.bookingSubmit(range, redirectAttrs);

        //THEN
        assertThat(result).isEqualTo("redirect:");
        assertThat(redirectAttrs.asMap().size()).isEqualTo(1);
        assertThat(redirectAttrs.getAttribute("bookingexception")).isNotNull();
        assertThat(redirectAttrs.getAttribute("bookingexception")).isEqualTo("No valid range provided: -1, 5 - Allowed are days in the " +
                "range of [0,364]");
        Mockito.verify(reservationService, Mockito.times(1)).bookARoom(range);
    }

    @Test
    public void should_test_isRoomXAvailableInRange_exception() throws InvalidRoomNumberException, InvalidRangeException {
        //GIVEN
        RangeAndRoomNumber rangeAndRoomNumber = new RangeAndRoomNumber();
        rangeAndRoomNumber.setRoomNumber(10000);
        rangeAndRoomNumber.setStart(0);
        rangeAndRoomNumber.setEnd(1);
        RedirectAttributes redirectAttrs = new RedirectAttributesModelMap();
        Mockito.when(reservationService.isRoomAvailableInRange(Mockito.anyInt(), Mockito.any())).thenThrow(new InvalidRoomNumberException(1337));

        //WHEN
        String result = testee.isRoomXAvailableInRange(rangeAndRoomNumber, redirectAttrs);

        //THEN
        assertThat(result).isEqualTo("redirect:");
        assertThat(redirectAttrs.asMap().size()).isEqualTo(1);
        assertThat(redirectAttrs.getAttribute("availabilityexception")).isNotNull();
        assertThat(redirectAttrs.getAttribute("availabilityexception")).isEqualTo("The Room with number 1337 does not exist.");
        Mockito.verify(reservationService, Mockito.times(1)).isRoomAvailableInRange(10000, new Range(0, 1));
    }


    @Test
    public void should_test_isRoomXAvailableInRange_true() throws InvalidRoomNumberException, InvalidRangeException {
        //GIVEN
        RangeAndRoomNumber rangeAndRoomNumber = new RangeAndRoomNumber();
        rangeAndRoomNumber.setRoomNumber(10000);
        rangeAndRoomNumber.setStart(0);
        rangeAndRoomNumber.setEnd(1);
        RedirectAttributes redirectAttrs = new RedirectAttributesModelMap();

        Mockito.when(reservationService.isRoomAvailableInRange(Mockito.anyInt(), Mockito.any())).thenReturn(true);

        //WHEN
        String result = testee.isRoomXAvailableInRange(rangeAndRoomNumber, redirectAttrs);

        //THEN
        assertThat(result).isEqualTo("redirect:");
        assertThat(redirectAttrs.asMap().size()).isEqualTo(1);
        assertThat(redirectAttrs.getAttribute("availabilityinfo")).isNotNull();
        assertThat(redirectAttrs.getAttribute("availabilityinfo")).isEqualTo("Room 10000 is available in range 0, 1");
        Mockito.verify(reservationService, Mockito.times(1)).isRoomAvailableInRange(10000, new Range(0, 1));
    }


    @Test
    public void should_test_isRoomXAvailableInRange_false() throws InvalidRoomNumberException, InvalidRangeException {
        //GIVEN
        RangeAndRoomNumber rangeAndRoomNumber = new RangeAndRoomNumber();
        rangeAndRoomNumber.setRoomNumber(10000);
        rangeAndRoomNumber.setStart(0);
        rangeAndRoomNumber.setEnd(1);
        RedirectAttributes redirectAttrs = new RedirectAttributesModelMap();

        Mockito.when(reservationService.isRoomAvailableInRange(Mockito.anyInt(), Mockito.any())).thenReturn(false);

        //WHEN
        String result = testee.isRoomXAvailableInRange(rangeAndRoomNumber, redirectAttrs);

        //THEN
        assertThat(result).isEqualTo("redirect:");
        assertThat(redirectAttrs.asMap().size()).isEqualTo(1);
        assertThat(redirectAttrs.getAttribute("availabilityinfo")).isNotNull();
        assertThat(redirectAttrs.getAttribute("availabilityinfo")).isEqualTo("Room 10000 is NOT available in range 0, 1");
        Mockito.verify(reservationService, Mockito.times(1)).isRoomAvailableInRange(10000, new Range(0, 1));
    }
}
