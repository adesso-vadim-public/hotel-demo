package de.adesso.vadim.demo.functionalinterface;

import de.adesso.vadim.demo.model.Range;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.stream.IntStream;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith({MockitoExtension.class})
@DisplayName("UnitTest Custom Predicates")
public class CustomPredicatesTest {

    private static IntStream validDaysStream() {
        return IntStream.range(0, 365);
    }

    private static Stream<Arguments> invalidRangesStream() {
        return Stream.of(
                Arguments.of(-100, -1), //outside left
                Arguments.of(-1, -1), //outside left same day
                Arguments.of(-1, 0), //overlapping Start
                Arguments.of(364, 365), //overlapping End
                Arguments.of(-1, 365), // overlapping Start and End
                Arguments.of(-1, 182), //overlapping Start - to middle
                Arguments.of(182, 365), //overlapping End - from middle
                Arguments.of(11, 10), //reversed Range
                Arguments.of(364, 0), //reversed Range
                Arguments.of(364, -1), //reversed Range outside
                Arguments.of(365, 400), //outside right
                Arguments.of(365, 365) //outside right same day
        );
    }

    private static Stream<Arguments> validRangesStream() {
        return Stream.of(
                Arguments.of(0, 0), // just the start
                Arguments.of(364, 364), // just the end
                Arguments.of(0, 364), // from start to end
                Arguments.of(50, 50), // inside single day
                Arguments.of(50, 250) // inside
        );
    }

    @ParameterizedTest
    @DisplayName("Testing isValidDay with values from 0 to 364")
    @MethodSource("validDaysStream")
    public void test_isValidDay_correct(int day) {
        //GIVEN

        //WHEN
        boolean isValid = CustomPredicates.isValidDay.test(day);

        //THEN
        assertThat(isValid).isTrue();
    }

    @Test
    @DisplayName("Testing isValidDay with value smaller 0")
    public void test_isValidDay_negativeDay() {
        //GIVEN

        //WHEN
        boolean isValid = CustomPredicates.isValidDay.test(-1);

        //THEN
        assertThat(isValid).isFalse();
    }

    @Test
    @DisplayName("Testing isValidDay with value smaller 0")
    public void test_isValidDay_nextYearDay() {
        //GIVEN

        //WHEN
        boolean isValid = CustomPredicates.isValidDay.test(365);

        //THEN
        assertThat(isValid).isFalse();
    }

    @ParameterizedTest
    @DisplayName("Testing isValidRange with invalid ranges")
    @MethodSource("invalidRangesStream")
    public void test_isValidRange_invalidRanges(int start, int end) {
        //GIVEN

        //WHEN
        boolean isValid = CustomPredicates.isValidRange.test(new Range(start, end));

        //THEN
        assertThat(isValid).isFalse();
    }

    @ParameterizedTest
    @DisplayName("Testing isValidRange with valid ranges")
    @MethodSource("validRangesStream")
    public void test_isValidRange_validRanges(int start, int end) {
        //GIVEN

        //WHEN
        boolean isValid = CustomPredicates.isValidRange.test(new Range(start, end));

        //THEN
        assertThat(isValid).isTrue();
    }

    @ParameterizedTest
    @DisplayName("Testing isNotValidRange with invalid ranges")
    @MethodSource("invalidRangesStream")
    public void test_isNotValidRange_invalidRanges(int start, int end) {
        //GIVEN

        //WHEN
        boolean isValid = CustomPredicates.isNotValidRange.test(new Range(start, end));

        //THEN
        assertThat(isValid).isTrue();
    }

    @ParameterizedTest
    @DisplayName("Testing isNotValidRange with valid ranges")
    @MethodSource("validRangesStream")
    public void test_isNotValidRange_validRanges(int start, int end) {
        //GIVEN

        //WHEN
        boolean isValid = CustomPredicates.isNotValidRange.test(new Range(start, end));

        //THEN
        assertThat(isValid).isFalse();
    }
}
