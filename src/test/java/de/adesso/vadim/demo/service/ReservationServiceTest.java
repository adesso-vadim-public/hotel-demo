package de.adesso.vadim.demo.service;

import de.adesso.vadim.demo.exception.InvalidRangeException;
import de.adesso.vadim.demo.exception.InvalidRoomNumberException;
import de.adesso.vadim.demo.exception.NoRoomAvailableException;
import de.adesso.vadim.demo.model.BookingResult;
import de.adesso.vadim.demo.model.Range;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith({MockitoExtension.class})
@DisplayName("UnitTest Reservation Service")
public class ReservationServiceTest {

    @InjectMocks
    private ReservationService testee;

    private static Stream<Arguments> invalidRangesStream() {
        return Stream.of(
                Arguments.of(-100, -1), //outside left
                Arguments.of(-1, -1), //outside left same day
                Arguments.of(-1, 0), //overlapping Start
                Arguments.of(364, 365), //overlapping End
                Arguments.of(-1, 365), // overlapping Start and End
                Arguments.of(-1, 182), //overlapping Start - to middle
                Arguments.of(182, 365), //overlapping End - from middle
                Arguments.of(11, 10), //reversed Range
                Arguments.of(364, 0), //reversed Range
                Arguments.of(364, -1), //reversed Range outside
                Arguments.of(365, 400), //outside right
                Arguments.of(365, 365) //outside right same day
        );
    }

    private static Stream<Arguments> validRangesStream() {
        return Stream.of(
                Arguments.of(0, 0), // just the start
                Arguments.of(364, 364), // just the end
                Arguments.of(0, 364), // from start to end
                Arguments.of(50, 250) // inside

        );
    }

    private static Stream<Arguments> validRangesOverlapping100to200Stream() {
        return Stream.of(
                Arguments.of(100, 200),
                Arguments.of(50, 200),
                Arguments.of(50, 201),
                Arguments.of(99, 201),
                Arguments.of(99, 100),
                Arguments.of(100, 100),
                Arguments.of(200, 200),
                Arguments.of(150, 150),
                Arguments.of(200, 201)
        );
    }

    private static Stream<Arguments> validRangesNonoverlapping100to200Stream() {
        return Stream.of(
                Arguments.of(0, 99),
                Arguments.of(201, 364),
                Arguments.of(300, 310),
                Arguments.of(50, 60),
                Arguments.of(99, 99),
                Arguments.of(201, 201)
        );
    }


    @Test
    void test_initialValues() {
        // GIVEN

        // WHEN

        // THEN
        assertThat(testee.numberOfRooms).isEqualTo(0);
        assertThat(testee.reservationStarts.size()).isEqualTo(365);
        assertThat(testee.reservationStarts.stream().mapToLong(Collection::size).sum()).isEqualTo(0);
        assertThat(testee.reservationEnds.size()).isEqualTo(365);
        assertThat(testee.reservationEnds.stream().mapToLong(Collection::size).sum()).isEqualTo(0);
    }

    @Test
    void test_setMaximumNumberOfRoomsAndClearBookings() {
        // GIVEN

        // WHEN
        testee.setMaximumNumberOfRoomsAndClearBookings(10);

        // THEN
        assertThat(testee.numberOfRooms).isEqualTo(10);
        assertThat(testee.reservationStarts.size()).isEqualTo(365);
        assertThat(testee.reservationStarts.stream().mapToLong(Collection::size).sum()).isEqualTo(0);
        assertThat(testee.reservationEnds.size()).isEqualTo(365);
        assertThat(testee.reservationEnds.stream().mapToLong(Collection::size).sum()).isEqualTo(0);
    }

    @ParameterizedTest
    @DisplayName("Testing getAvailableLowestRoomNumberInRange with valid ranges, without any reservations, 1 room max")
    @MethodSource("validRangesStream")
    void test_getAvailableLowestRoomNumberInRange_validRanges(int start, int end) throws InvalidRangeException, NoRoomAvailableException {
        // GIVEN
        testee.setMaximumNumberOfRoomsAndClearBookings(1);

        // WHEN
        int availableRoomNumber = testee.getAvailableLowestRoomNumberInRange(new Range(start, end));

        // THEN
        assertThat(testee.numberOfRooms).isEqualTo(1);
        assertThat(testee.reservationStarts.size()).isEqualTo(365);
        assertThat(testee.reservationStarts.stream().mapToLong(Collection::size).sum()).isEqualTo(0);
        assertThat(testee.reservationEnds.size()).isEqualTo(365);
        assertThat(testee.reservationEnds.stream().mapToLong(Collection::size).sum()).isEqualTo(0);
        assertThat(availableRoomNumber).isEqualTo(1);
    }


    @ParameterizedTest
    @DisplayName("Testing getAvailableLowestRoomNumberInRange with invalid ranges, without any reservations, 1 room max")
    @MethodSource("invalidRangesStream")
    void test_getAvailableLowestRoomNumberInRange_invalidRanges(int start, int end) {
        // GIVEN
        testee.setMaximumNumberOfRoomsAndClearBookings(1);

        // WHEN
        InvalidRangeException invalidRangeException = Assertions.assertThrows(InvalidRangeException.class,
                () -> testee.getAvailableLowestRoomNumberInRange(new Range(start, end)));

        // THEN
        assertThat(testee.numberOfRooms).isEqualTo(1);
        assertThat(testee.reservationStarts.size()).isEqualTo(365);
        assertThat(testee.reservationStarts.stream().mapToLong(Collection::size).sum()).isEqualTo(0);
        assertThat(testee.reservationEnds.size()).isEqualTo(365);
        assertThat(testee.reservationEnds.stream().mapToLong(Collection::size).sum()).isEqualTo(0);
        assertThat(invalidRangeException.getMessage()).isEqualTo(String.format("No valid range provided: %d, %d - Allowed are days in the" +
                        " range of [0,364]",
                start, end));
    }


    @ParameterizedTest
    @DisplayName("Testing getAvailableLowestRoomNumberInRange with valid ranges, without any reservations, 0 room max")
    @MethodSource("validRangesStream")
    void test_getAvailableLowestRoomNumberInRange_noRooms(int start, int end) {
        // GIVEN

        // WHEN
        NoRoomAvailableException noRoomAvailableException = Assertions.assertThrows(NoRoomAvailableException.class,
                () -> testee.getAvailableLowestRoomNumberInRange(new Range(start, end)));

        // THEN
        assertThat(testee.numberOfRooms).isEqualTo(0);
        assertThat(testee.reservationStarts.size()).isEqualTo(365);
        assertThat(testee.reservationStarts.stream().mapToLong(Collection::size).sum()).isEqualTo(0);
        assertThat(testee.reservationEnds.size()).isEqualTo(365);
        assertThat(testee.reservationEnds.stream().mapToLong(Collection::size).sum()).isEqualTo(0);
        assertThat(noRoomAvailableException.getMessage()).isEqualTo(String.format("No room available in range %d, %d", start,
                end));
    }

    @ParameterizedTest
    @DisplayName("Testing getAvailableLowestRoomNumberInRange with valid ranges, with 1 reservation, 1 room max")
    @MethodSource("validRangesStream")
    void test_getAvailableLowestRoomNumberInRange_noRoomsBecauseBooked(int start, int end) throws InvalidRangeException,
            NoRoomAvailableException {
        // GIVEN
        testee.setMaximumNumberOfRoomsAndClearBookings(1);
        testee.bookARoom(new Range(0, 364));

        // WHEN
        NoRoomAvailableException noRoomAvailableException = Assertions.assertThrows(NoRoomAvailableException.class,
                () -> testee.getAvailableLowestRoomNumberInRange(new Range(start, end)));

        // THEN
        assertThat(testee.numberOfRooms).isEqualTo(1);
        assertThat(testee.reservationStarts.size()).isEqualTo(365);
        assertThat(testee.reservationStarts.stream().mapToLong(Collection::size).sum()).isEqualTo(1);
        assertThat(testee.reservationEnds.size()).isEqualTo(365);
        assertThat(testee.reservationEnds.stream().mapToLong(Collection::size).sum()).isEqualTo(1);
        assertThat(noRoomAvailableException.getMessage()).isEqualTo(String.format("No room available in range %d, %d", start,
                end));
    }

    @ParameterizedTest
    @DisplayName("Testing getAvailableLowestRoomNumberInRange with overlapping ranges, with 1 reservation, 1 room max")
    @MethodSource("validRangesOverlapping100to200Stream")
    void test_getAvailableLowestRoomNumberInRange_overlapping(int start, int end) throws InvalidRangeException, NoRoomAvailableException {
        // GIVEN
        testee.setMaximumNumberOfRoomsAndClearBookings(1);
        testee.bookARoom(new Range(100, 200));

        // WHEN
        NoRoomAvailableException noRoomAvailableException = Assertions.assertThrows(NoRoomAvailableException.class,
                () -> testee.getAvailableLowestRoomNumberInRange(new Range(start, end)));

        // THEN
        assertThat(testee.numberOfRooms).isEqualTo(1);
        assertThat(testee.reservationStarts.size()).isEqualTo(365);
        assertThat(testee.reservationStarts.stream().mapToLong(Collection::size).sum()).isEqualTo(1);
        assertThat(testee.reservationEnds.size()).isEqualTo(365);
        assertThat(testee.reservationEnds.stream().mapToLong(Collection::size).sum()).isEqualTo(1);
        assertThat(noRoomAvailableException.getMessage()).isEqualTo(String.format("No room available in range %d, %d", start,
                end));
    }

    @ParameterizedTest
    @DisplayName("Testing getAvailableLowestRoomNumberInRange with nonoverlapping ranges, with 1 reservation, 1 room max")
    @MethodSource("validRangesNonoverlapping100to200Stream")
    void test_getAvailableLowestRoomNumberInRange_nonoverlapping(int start, int end) throws InvalidRangeException,
            NoRoomAvailableException {
        // GIVEN
        testee.setMaximumNumberOfRoomsAndClearBookings(1);
        testee.bookARoom(new Range(100, 200));

        // WHEN
        int availableRoomNumber = testee.getAvailableLowestRoomNumberInRange(new Range(start, end));

        // THEN
        assertThat(testee.numberOfRooms).isEqualTo(1);
        assertThat(testee.reservationStarts.size()).isEqualTo(365);
        assertThat(testee.reservationStarts.stream().mapToLong(Collection::size).sum()).isEqualTo(1);
        assertThat(testee.reservationEnds.size()).isEqualTo(365);
        assertThat(testee.reservationEnds.stream().mapToLong(Collection::size).sum()).isEqualTo(1);
        assertThat(availableRoomNumber).isEqualTo(1);
    }


    @Test
    void test_bookARoom_multiple() throws InvalidRangeException, NoRoomAvailableException {
        // GIVEN
        testee.setMaximumNumberOfRoomsAndClearBookings(5);

        // WHEN
        testee.bookARoom(new Range(100, 200));
        testee.bookARoom(new Range(101, 200));
        testee.bookARoom(new Range(100, 201));
        testee.bookARoom(new Range(201, 300));
        testee.bookARoom(new Range(201, 201));

        // THEN
        assertThat(testee.numberOfRooms).isEqualTo(5);
        assertThat(testee.reservationStarts.size()).isEqualTo(365);
        assertThat(testee.reservationStarts.stream().mapToLong(Collection::size).sum()).isEqualTo(5);
        assertThat(testee.reservationStarts.get(100)).isEqualTo(Set.of(1, 3));
        assertThat(testee.reservationStarts.get(101)).isEqualTo(Set.of(2));
        assertThat(testee.reservationStarts.get(201)).isEqualTo(Set.of(1, 2));
        assertThat(testee.reservationEnds.size()).isEqualTo(365);
        assertThat(testee.reservationEnds.stream().mapToLong(Collection::size).sum()).isEqualTo(5);
        assertThat(testee.reservationEnds.get(200)).isEqualTo(Set.of(1, 2));
        assertThat(testee.reservationEnds.get(201)).isEqualTo(Set.of(2, 3));
        assertThat(testee.reservationEnds.get(300)).isEqualTo(Set.of(1));
    }


    @Test
    void test_bookARoom_multiple_until_error() throws InvalidRangeException, NoRoomAvailableException {
        // GIVEN
        testee.setMaximumNumberOfRoomsAndClearBookings(5);

        // WHEN
        testee.bookARoom(new Range(100, 200));
        testee.bookARoom(new Range(100, 200));
        testee.bookARoom(new Range(100, 200));
        testee.bookARoom(new Range(100, 200));
        testee.bookARoom(new Range(100, 200));
        NoRoomAvailableException noRoomAvailableException = Assertions.assertThrows(NoRoomAvailableException.class,
                () -> testee.bookARoom(new Range(100, 200)));

        // THEN
        assertThat(testee.numberOfRooms).isEqualTo(5);
        assertThat(testee.reservationStarts.size()).isEqualTo(365);
        assertThat(testee.reservationStarts.stream().mapToLong(Collection::size).sum()).isEqualTo(5);
        assertThat(testee.reservationStarts.get(100)).isEqualTo(Set.of(1, 2, 3, 4, 5));
        assertThat(testee.reservationEnds.size()).isEqualTo(365);
        assertThat(testee.reservationEnds.stream().mapToLong(Collection::size).sum()).isEqualTo(5);
        assertThat(testee.reservationEnds.get(200)).isEqualTo(Set.of(1, 2, 3, 4, 5));
        assertThat(noRoomAvailableException.getMessage()).isEqualTo("No room available in range 100, 200");
    }

    @Test
    void test_bookARoom_eachDayTwice() throws InvalidRangeException, NoRoomAvailableException {
        // GIVEN
        testee.setMaximumNumberOfRoomsAndClearBookings(2);

        // WHEN
        for (int loop = 0; loop < 2; loop++) {
            for (int i = 0; i < 365; i++) {
                testee.bookARoom(new Range(i, i));
            }
        }

        // THEN
        assertThat(testee.numberOfRooms).isEqualTo(2);
        assertThat(testee.reservationStarts.size()).isEqualTo(365);
        assertThat(testee.reservationStarts.stream().mapToLong(Collection::size).sum()).isEqualTo(730);
        assertThat(testee.reservationEnds.size()).isEqualTo(365);
        assertThat(testee.reservationEnds.stream().mapToLong(Collection::size).sum()).isEqualTo(730);
        for (int i = 0; i < 365; i++) {
            assertThat(testee.reservationStarts.get(i)).isEqualTo(Set.of(1, 2));
            assertThat(testee.reservationEnds.get(i)).isEqualTo(Set.of(1, 2));
        }
    }


    @Test
    void test_getAvailableLowestRoomNumberInRange() throws InvalidRangeException, NoRoomAvailableException {
        // GIVEN
        testee.setMaximumNumberOfRoomsAndClearBookings(5);

        // WHEN
        int lowestRoomnumberWhenNothingBooked1 = testee.getAvailableLowestRoomNumberInRange(new Range(0, 0));
        int lowestRoomnumberWhenNothingBooked2 = testee.getAvailableLowestRoomNumberInRange(new Range(0, 364));
        int lowestRoomnumberWhenNothingBooked3 = testee.getAvailableLowestRoomNumberInRange(new Range(364, 364));
        int lowestRoomnumberWhenNothingBooked4 = testee.getAvailableLowestRoomNumberInRange(new Range(150, 150));
        testee.bookARoom(new Range(100, 200));
        int lowestRoomnumberAfter1Booked = testee.getAvailableLowestRoomNumberInRange(new Range(150, 150));
        testee.bookARoom(new Range(100, 200));
        int lowestRoomnumberAfter2Booked = testee.getAvailableLowestRoomNumberInRange(new Range(150, 150));
        testee.bookARoom(new Range(100, 200));
        int lowestRoomnumberAfter3Booked = testee.getAvailableLowestRoomNumberInRange(new Range(150, 150));
        testee.bookARoom(new Range(100, 200));
        int lowestRoomnumberAfter4Booked = testee.getAvailableLowestRoomNumberInRange(new Range(150, 150));
        testee.bookARoom(new Range(100, 200));
        NoRoomAvailableException noRoomAvailableException = Assertions.assertThrows(NoRoomAvailableException.class,
                () -> testee.getAvailableLowestRoomNumberInRange(new Range(150, 150)));

        // overlapBooking
        testee.bookARoom(new Range(300, 310));
        int lowestRoomnumberAfterOverlap1 = testee.getAvailableLowestRoomNumberInRange(new Range(300, 310));
        testee.bookARoom(new Range(301, 310));
        int lowestRoomnumberAfterOverlap2 = testee.getAvailableLowestRoomNumberInRange(new Range(300, 310));
        testee.bookARoom(new Range(300, 314));
        int lowestRoomnumberAfterOverlap3 = testee.getAvailableLowestRoomNumberInRange(new Range(305, 312));
        testee.bookARoom(new Range(311, 315));
        int lowestRoomnumberAfterOverlap4 = testee.getAvailableLowestRoomNumberInRange(new Range(312, 315));
        testee.bookARoom(new Range(305, 320));
        int lowestRoomnumberAfterOverlap5 = testee.getAvailableLowestRoomNumberInRange(new Range(300, 350));


        // THEN
        assertThat(lowestRoomnumberWhenNothingBooked1).isEqualTo(1);
        assertThat(lowestRoomnumberWhenNothingBooked2).isEqualTo(1);
        assertThat(lowestRoomnumberWhenNothingBooked3).isEqualTo(1);
        assertThat(lowestRoomnumberWhenNothingBooked4).isEqualTo(1);

        assertThat(lowestRoomnumberAfter1Booked).isEqualTo(2);
        assertThat(lowestRoomnumberAfter2Booked).isEqualTo(3);
        assertThat(lowestRoomnumberAfter3Booked).isEqualTo(4);
        assertThat(lowestRoomnumberAfter4Booked).isEqualTo(5);
        assertThat(noRoomAvailableException.getMessage()).isEqualTo("No room available in range 150, 150");

        assertThat(lowestRoomnumberAfterOverlap1).isEqualTo(2);
        assertThat(lowestRoomnumberAfterOverlap2).isEqualTo(3);
        assertThat(lowestRoomnumberAfterOverlap3).isEqualTo(4);
        assertThat(lowestRoomnumberAfterOverlap4).isEqualTo(2);
        assertThat(lowestRoomnumberAfterOverlap5).isEqualTo(5);
    }


    @Test
    @DisplayName("allRoomBookedDays based on Demo 5: Complex Requests (Size=2)")
    void test_allRoomBookedDays() throws NoRoomAvailableException, InvalidRangeException {
        // GIVEN
        testee.setMaximumNumberOfRoomsAndClearBookings(2);
        testee.bookARoom(new Range(1, 3));
        testee.bookARoom(new Range(0, 4));
        testee.bookARoom(new Range(5, 5));
        testee.bookARoom(new Range(10, 10));
        testee.bookARoom(new Range(6, 7));
        testee.bookARoom(new Range(8, 10));
        testee.bookARoom(new Range(8, 9));
        boolean[][] expected = new boolean[2][];
        expected[0] = new boolean[365];
        expected[1] = new boolean[365];
        expected[0][1] = true;
        expected[0][2] = true;
        expected[0][3] = true;
        expected[0][5] = true;
        expected[0][10] = true;
        expected[0][6] = true;
        expected[0][7] = true;
        expected[0][8] = true;
        expected[0][9] = true;

        expected[1][0] = true;
        expected[1][1] = true;
        expected[1][2] = true;
        expected[1][3] = true;
        expected[1][4] = true;
        expected[1][8] = true;
        expected[1][9] = true;
        expected[1][10] = true;

        // WHEN
        boolean[][] calculatedBookedDays = testee.allRoomBookedDays();

        // THEN
        assertThat(testee.numberOfRooms).isEqualTo(2);
        assertThat(calculatedBookedDays).isEqualTo(expected);
    }


    @Test
    @DisplayName("isRoomAvailableInRange for various requests")
    void test_isRoomAvailableInRange() throws NoRoomAvailableException, InvalidRangeException, InvalidRoomNumberException {
        // GIVEN
        testee.setMaximumNumberOfRoomsAndClearBookings(2);

        // WHEN
        testee.bookARoom(new Range(1, 3));
        testee.bookARoom(new Range(0, 4));
        boolean firstRoomD0D0Availability = testee.isRoomAvailableInRange(1, new Range(0, 0));
        boolean secondRoomD0D0Availability = testee.isRoomAvailableInRange(2, new Range(0, 0));
        boolean firstRoomD1D1Availability = testee.isRoomAvailableInRange(1, new Range(1, 1));
        boolean secondRoomD5D10Availability = testee.isRoomAvailableInRange(2, new Range(5, 10));

        InvalidRangeException invalidRangeException = Assertions.assertThrows(InvalidRangeException.class,
                () -> testee.isRoomAvailableInRange(2, new Range(5, 4)));

        InvalidRoomNumberException invalidRoomNumberException = Assertions.assertThrows(InvalidRoomNumberException.class,
                () -> testee.isRoomAvailableInRange(2000, new Range(5, 10)));

        // THEN
        assertThat(testee.numberOfRooms).isEqualTo(2);
        assertThat(firstRoomD0D0Availability).isTrue();
        assertThat(secondRoomD0D0Availability).isFalse();
        assertThat(firstRoomD1D1Availability).isFalse();
        assertThat(secondRoomD5D10Availability).isTrue();
        assertThat(invalidRangeException.getMessage()).isEqualTo("No valid range provided: 5, 4 - Allowed are days in the range of [0," +
                "364]");
        assertThat(invalidRoomNumberException.getMessage()).isEqualTo("The Room with number 2000 does not exist.");
    }


    @Test
    void test_bookARoom_writingBookingResults() throws InvalidRangeException, NoRoomAvailableException {
        // GIVEN
        testee.setMaximumNumberOfRoomsAndClearBookings(5);

        // WHEN
        testee.bookARoom(new Range(100, 200));
        testee.bookARoom(new Range(101, 203));
        Assertions.assertThrows(InvalidRangeException.class,
                () -> testee.bookARoom(new Range(5, -10)));

        List<BookingResult> writtenResults = testee.getBookingResults();

        // THEN
        assertThat(writtenResults).isNotNull();
        assertThat(writtenResults.size()).isEqualTo(3);
        assertThat(writtenResults.get(0)).isEqualTo(new BookingResult(100, 200, true, 1, ""));
        assertThat(writtenResults.get(1)).isEqualTo(new BookingResult(101, 203, true, 2, ""));
        assertThat(writtenResults.get(2)).isEqualTo(new BookingResult(5, -10, false, -1, "No valid range provided: 5, -10 - Allowed are " +
                "days " +
                "in the range of [0,364]"));

    }

    
    @Test
    @DisplayName("Demo 1a: Requests outside our planning period are declined (Size=1) - negative start")
    void test_Demo1a() {
        // GIVEN
        testee.setMaximumNumberOfRoomsAndClearBookings(1);

        // WHEN
        InvalidRangeException invalidRangeException = Assertions.assertThrows(InvalidRangeException.class,
                () -> testee.bookARoom(new Range(-4, 2)));

        // THEN
        assertThat(testee.numberOfRooms).isEqualTo(1);
        assertThat(invalidRangeException.getMessage()).isEqualTo("No valid range provided: -4, 2 - Allowed are days in the range of [0," +
                "364]");
    }

    @Test
    @DisplayName("Demo 1b: Requests outside our planning period are declined (Size=1) - end out of range")
    void test_Demo1b() {
        // GIVEN
        testee.setMaximumNumberOfRoomsAndClearBookings(1);

        // WHEN
        InvalidRangeException invalidRangeException = Assertions.assertThrows(InvalidRangeException.class,
                () -> testee.bookARoom(new Range(200, 400)));

        // THEN
        assertThat(testee.numberOfRooms).isEqualTo(1);
        assertThat(invalidRangeException.getMessage()).isEqualTo("No valid range provided: 200, 400 - Allowed are days in the range of " +
                "[0,364]");
    }

    @Test
    @DisplayName("Demo 2: Requests are accepted (Size=3)")
    void test_Demo2() throws NoRoomAvailableException, InvalidRangeException {
        // GIVEN
        testee.setMaximumNumberOfRoomsAndClearBookings(3);

        // WHEN
        testee.bookARoom(new Range(0, 5));
        testee.bookARoom(new Range(7, 13));
        testee.bookARoom(new Range(3, 9));
        testee.bookARoom(new Range(5, 7));
        testee.bookARoom(new Range(6, 6));
        testee.bookARoom(new Range(0, 4));

        // THEN
        assertThat(testee.numberOfRooms).isEqualTo(3);
    }

    @Test
    @DisplayName("Demo 3: Requests are declined (Size=3)")
    void test_Demo3() throws NoRoomAvailableException, InvalidRangeException {
        // GIVEN
        testee.setMaximumNumberOfRoomsAndClearBookings(3);

        // WHEN
        testee.bookARoom(new Range(1, 3));
        testee.bookARoom(new Range(2, 5));
        testee.bookARoom(new Range(1, 9));
        NoRoomAvailableException noRoomAvailableException = Assertions.assertThrows(NoRoomAvailableException.class,
                () -> testee.bookARoom(new Range(0, 15)));

        // THEN
        assertThat(testee.numberOfRooms).isEqualTo(3);
        assertThat(noRoomAvailableException.getMessage()).isEqualTo("No room available in range 0, 15");
    }


    @Test
    @DisplayName("Demo 4: Requests can be accepted after a decline (Size=3)")
    void test_Demo4() throws NoRoomAvailableException, InvalidRangeException {
        // GIVEN
        testee.setMaximumNumberOfRoomsAndClearBookings(3);

        // WHEN
        testee.bookARoom(new Range(1, 3));
        testee.bookARoom(new Range(0, 15));
        testee.bookARoom(new Range(1, 9));
        NoRoomAvailableException noRoomAvailableException = Assertions.assertThrows(NoRoomAvailableException.class,
                () -> testee.bookARoom(new Range(2, 5)));
        testee.bookARoom(new Range(4, 9));

        // THEN
        assertThat(testee.numberOfRooms).isEqualTo(3);
        assertThat(noRoomAvailableException.getMessage()).isEqualTo("No room available in range 2, 5");
    }

    @Test
    @DisplayName("Demo 5: Complex Requests (Size=2)")
    void test_Demo5() throws NoRoomAvailableException, InvalidRangeException {
        // GIVEN
        testee.setMaximumNumberOfRoomsAndClearBookings(2);

        // WHEN
        testee.bookARoom(new Range(1, 3));
        testee.bookARoom(new Range(0, 4));
        NoRoomAvailableException noRoomAvailableExceptionD2D3 = Assertions.assertThrows(NoRoomAvailableException.class,
                () -> testee.bookARoom(new Range(2, 3)));
        testee.bookARoom(new Range(5, 5));
        NoRoomAvailableException noRoomAvailableExceptionD4D10 = Assertions.assertThrows(NoRoomAvailableException.class,
                () -> testee.bookARoom(new Range(4, 10)));
        testee.bookARoom(new Range(10, 10));
        testee.bookARoom(new Range(6, 7));
        testee.bookARoom(new Range(8, 10));
        testee.bookARoom(new Range(8, 9));

        // THEN
        assertThat(testee.numberOfRooms).isEqualTo(2);
        assertThat(noRoomAvailableExceptionD2D3.getMessage()).isEqualTo("No room available in range 2, 3");
        assertThat(noRoomAvailableExceptionD4D10.getMessage()).isEqualTo("No room available in range 4, 10");
    }


}
