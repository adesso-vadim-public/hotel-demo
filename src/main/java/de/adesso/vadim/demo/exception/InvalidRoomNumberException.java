package de.adesso.vadim.demo.exception;

public class InvalidRoomNumberException extends Exception {
    public InvalidRoomNumberException(int roomNumber) {
        super(String.format("The Room with number %d does not exist.", roomNumber));
    }
}
