package de.adesso.vadim.demo.exception;

import de.adesso.vadim.demo.model.Range;

public class InvalidRangeException extends Exception {
    public InvalidRangeException(Range range) {
        super(String.format("No valid range provided: %d, %d - Allowed are days in the range of [0,364]", range.getStart(),
                range.getEnd()));
    }
}
