package de.adesso.vadim.demo.exception;

import de.adesso.vadim.demo.model.Range;

public class NoRoomAvailableException extends Exception {
    public NoRoomAvailableException(Range range) {

        super(String.format("No room available in range %d, %d", range.getStart(), range.getEnd()));
    }
}
