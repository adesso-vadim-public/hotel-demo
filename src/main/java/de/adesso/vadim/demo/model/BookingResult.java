package de.adesso.vadim.demo.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class BookingResult {
    int start;
    int end;
    boolean accepted;
    int roomNumber;
    String errorReason;

    public BookingResult(Range range, boolean accepted, int roomNumber, String errorReason) {
        this(range.getStart(), range.getEnd(), accepted, roomNumber, errorReason);
    }
}
