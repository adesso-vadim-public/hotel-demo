package de.adesso.vadim.demo.api;

import de.adesso.vadim.demo.model.Range;
import de.adesso.vadim.demo.model.RangeAndRoomNumber;
import de.adesso.vadim.demo.model.SimpleNumber;
import de.adesso.vadim.demo.service.ReservationService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
@Slf4j
@RequiredArgsConstructor
public class BookingController {

    private final ReservationService reservationService;

    @PostMapping("/booking/maxrooms")
    public String setMaxRooms(@ModelAttribute SimpleNumber maxrooms) {
        reservationService.setMaximumNumberOfRoomsAndClearBookings(maxrooms.getNumber());
        return "redirect:";
    }

    @PostMapping("/booking/book")
    public String bookingSubmit(@ModelAttribute Range range, RedirectAttributes redirectAttrs) {
        try {
            reservationService.bookARoom(range);
        } catch (Exception e) {
            redirectAttrs.addAttribute("bookingexception", e.getMessage());
            log.error(e.getMessage());
        }
        return "redirect:";
    }

    @PostMapping("/booking/availability")
    public String isRoomXAvailableInRange(@ModelAttribute RangeAndRoomNumber availabilitychecker,
                                          RedirectAttributes redirectAttrs) {
        try {
            boolean isRoomAvailableInRange = reservationService.isRoomAvailableInRange(availabilitychecker.getRoomNumber(),
                    new Range(availabilitychecker.getStart(), availabilitychecker.getEnd()));
            redirectAttrs.addAttribute("availabilityinfo", String.format("Room %d is %savailable in range %d, %d",
                    availabilitychecker.getRoomNumber(),
                    isRoomAvailableInRange ? "" : "NOT ",
                    availabilitychecker.getStart(), availabilitychecker.getEnd()));
        } catch (Exception e) {
            redirectAttrs.addAttribute("availabilityexception", e.getMessage());
            log.error(e.getMessage());
        }
        return "redirect:";
    }

    @GetMapping("/booking")
    public String bookingForm(Model model, @ModelAttribute("bookingexception") String bookingException,
                              @ModelAttribute("availabilityinfo") String availabilityinfo,
                              @ModelAttribute("availabilityexception") String availabilityexception) {
        model.addAttribute("bookingexception", bookingException);
        model.addAttribute("availabilityinfo", availabilityinfo);
        model.addAttribute("availabilityexception", availabilityexception);
        model.addAttribute("maxrooms", new SimpleNumber());
        model.addAttribute("range", new Range());
        model.addAttribute("bookingResults", reservationService.getBookingResults());
        model.addAttribute("availabilitychecker", new RangeAndRoomNumber());
        return "booking";
    }

    @GetMapping("/booking/currentbooked")
    public String getCurrentlyBooked(Model model) {
        model.addAttribute("allbookeddays", reservationService.allRoomBookedDays());
        return "bookingtable";
    }

}
