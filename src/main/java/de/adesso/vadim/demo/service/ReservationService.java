package de.adesso.vadim.demo.service;

import de.adesso.vadim.demo.exception.InvalidRangeException;
import de.adesso.vadim.demo.exception.InvalidRoomNumberException;
import de.adesso.vadim.demo.exception.NoRoomAvailableException;
import de.adesso.vadim.demo.functionalinterface.CustomPredicates;
import de.adesso.vadim.demo.model.BookingResult;
import de.adesso.vadim.demo.model.Range;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Slf4j
@Service
public class ReservationService {

    List<Set<Integer>> reservationStarts;
    List<Set<Integer>> reservationEnds;
    List<BookingResult> bookingResults;

    int numberOfRooms; // room Numbers begin from 1 up to numberOfRooms(inclusive)


    public ReservationService() {
        setMaximumNumberOfRoomsAndClearBookings(0);
    }

    public void setMaximumNumberOfRoomsAndClearBookings(int numberOfRooms) {
        // negative values|values > 1000 are not caught as there should be no problem with it
        //  =>negative values lead to non-bookable rooms

        // setting the maximum number also deletes all bookings

        log.info("Setting max number of rooms to: " + numberOfRooms);
        this.numberOfRooms = numberOfRooms;
        log.info("Deleting bookings.");
        reservationStarts =
                IntStream.range(0, 365).mapToObj((i) -> new HashSet<Integer>()).collect(Collectors.toList());
        reservationEnds =
                IntStream.range(0, 365).mapToObj((i) -> new HashSet<Integer>()).collect(Collectors.toList());
        log.info("Resetting booking results.");
        bookingResults = new ArrayList<>();
    }

    public void bookARoom(Range range) throws InvalidRangeException, NoRoomAvailableException {
        try {
            int availabeRoomNumberInRange = getAvailableLowestRoomNumberInRange(range);
            reservationStarts.get(range.getStart()).add(availabeRoomNumberInRange);
            reservationEnds.get(range.getEnd()).add(availabeRoomNumberInRange);
            bookingResults.add(new BookingResult(range, true, availabeRoomNumberInRange, ""));
        } catch (Exception e) {
            bookingResults.add(new BookingResult(range, false, -1, e.getMessage()));
            throw e;
        }
    }

    public List<BookingResult> getBookingResults() {
        return bookingResults;
    }

    public int getAvailableLowestRoomNumberInRange(Range range) throws InvalidRangeException, NoRoomAvailableException {
        if (CustomPredicates.isNotValidRange.test(range)) {
            throw new InvalidRangeException(range);
        }

        Set<Integer> blockedRooms = getBlockedRoomNumbersInRange(range);

        for (int roomNumber = 1; roomNumber < numberOfRooms + 1; roomNumber++) {
            if (!blockedRooms.contains(roomNumber)) {
                return roomNumber;
            }
        }
        throw new NoRoomAvailableException(range);
    }

    private Set<Integer> getBlockedRoomNumbersInRange(Range range) throws InvalidRangeException {
        if (CustomPredicates.isNotValidRange.test(range)) {
            throw new InvalidRangeException(range);
        }
        Set<Integer> blockedRooms = new HashSet<>();
        for (int i = 0; i < range.getStart(); i++) {
            blockedRooms.addAll(reservationStarts.get(i));
            blockedRooms.removeAll(reservationEnds.get(i));
        }

        for (int i = range.getStart(); i < range.getEnd() + 1; i++) {
            blockedRooms.addAll(reservationStarts.get(i));
        }
        return blockedRooms;
    }

    public boolean isRoomAvailableInRange(int roomNumber, Range range) throws InvalidRangeException, InvalidRoomNumberException {
        throwErrorOnInvalidRoomNumber(roomNumber);
        return !getBlockedRoomNumbersInRange(range).contains(roomNumber);
    }


    public boolean[] roomBookedDays(int roomNumber) {
        boolean[] bookedOnDay = new boolean[365];
        boolean currentlyBooked = false;
        for (int i = 0; i < 365; i++) {
            if (!currentlyBooked && reservationStarts.get(i).contains(roomNumber)) {
                currentlyBooked = true;
            }
            bookedOnDay[i] = currentlyBooked;
            if (currentlyBooked && reservationEnds.get(i).contains(roomNumber)) {
                currentlyBooked = false;
            }
        }
        return bookedOnDay;
    }

    public boolean[][] allRoomBookedDays() {
        boolean[][] allRoomBookedDays = new boolean[numberOfRooms][];
        for (int i = 1; i <= numberOfRooms; i++) {
            allRoomBookedDays[i - 1] = roomBookedDays(i);
        }
        return allRoomBookedDays;
    }

    void throwErrorOnInvalidRoomNumber(int roomNumber) throws InvalidRoomNumberException {
        if (roomNumber <= 0 || roomNumber > numberOfRooms) {
            throw new InvalidRoomNumberException(roomNumber);
        }


    }

}
