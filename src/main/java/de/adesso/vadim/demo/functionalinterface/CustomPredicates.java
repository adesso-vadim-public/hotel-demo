package de.adesso.vadim.demo.functionalinterface;


import de.adesso.vadim.demo.model.Range;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.util.function.Predicate;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class CustomPredicates {


    public static final Predicate<Integer> isValidDay = (Integer day) ->
            day >= 0 && day < 365;
    
    public static final Predicate<Range> isValidRange = (Range range) ->
            range.getEnd() >= range.getStart() && isValidDay.test(range.getEnd()) && isValidDay.test(range.getStart());

    public static final Predicate<Range> isNotValidRange = isValidRange.negate();


}
